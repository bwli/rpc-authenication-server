package com.timaven.auth;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

// When Maven build app, it will still run tests inside this class
// but will not run SpringBoot app, so will not test connection to DB and build will be successful.
//@SpringBootTest
class TimeAuthApplicationTests {

    @Test
    void contextLoads() {
    }

}
