create table if not exists auth.oauth_code
(
    code VARCHAR(256),
    authentication bytea
);
