update public.oauth_client_details
set authorized_grant_types = 'authorization_code,refresh_token',
    access_token_validity  = 28800
where client_id = 'timaven-client';