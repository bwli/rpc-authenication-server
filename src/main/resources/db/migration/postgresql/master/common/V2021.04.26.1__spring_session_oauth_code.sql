drop table if exists auth.spring_session cascade;
drop table if exists auth.spring_session_attributes cascade;
drop table if exists auth.oauth_code cascade;