CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- oauth_client_details cannot have schema. Otherwise, server won't use it

INSERT INTO public.oauth_client_details
(client_id, client_secret, scope, authorized_grant_types, access_token_validity,
 web_server_redirect_uri, autoapprove)
VALUES ('timaven-client',
        crypt('timaven-secret', gen_salt('bf', 10)),
        'user_info',
        'authorization_code', 7200,
        'http://localhost:8082/timaven/login',
        true)
ON CONFLICT (client_id) do update set web_server_redirect_uri = 'http://localhost:8082/timaven/login';

DROP EXTENSION IF EXISTS pgcrypto;
