INSERT INTO auth.role (name, display_name, description, display_order)
VALUES ('ROLE_corporate_timekeeper', 'Corporate TK', 'Manage corporate stuff timesheets', 35)
ON CONFLICT DO NOTHING;

update auth.role
set display_name = 'Corporate Payroll'
where name = 'ROLE_corporate_accountant';
