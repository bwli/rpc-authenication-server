DO
$$
    BEGIN
            update auth.role
            set description = '1. Daily time sheet operation including allocate cost code, report rain out, manage exception and manage per diem if manual. 2. Punch record upload. 3. Project roster management. Daily timesheet approval. 4. Weekly timesheet processing including reconciling OT and per diem'
            where name = 'ROLE_accountant_foreman';
    END ;
$$;
