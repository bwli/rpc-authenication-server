create table if not exists auth.role
(
    id           bigserial primary key,
    name         varchar unique not null,
    display_name varchar unique not null  default '',
    description  varchar,
    created_at   timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'role'
                        and table_schema = 'auth'
                        and column_name = 'display_name')
        then alter table auth.role
            add column if not exists display_name varchar not null default '';

            insert into auth.role (name, display_name, description, display_order)
            values ('ROLE_it_admin', 'IT Admin', 'IT Admin', -1)
            on conflict do nothing;

            update auth.role
            set display_name = 'Super Admin'
            where name = 'ROLE_super_admin';

            update auth.role
            set display_name = 'Corporate Admin'
            where name = 'ROLE_admin';

            update auth.role
            set display_name = 'Corporate TK'
            where name = 'ROLE_corporate_accountant';

            update auth.role
            set display_name = 'Field Accounting Manager'
            where name = 'ROLE_project_manager';

            update auth.role
            set display_name = 'Field TK'
            where name in ('ROLE_accountant_foreman', 'ROLE_accountant');

            update auth.role
            set display_name = 'Field Clerk'
            where name = 'ROLE_timekeeper';

            update auth.role
            set display_name = 'Foreman'
            where name = 'ROLE_foreman';

            update auth.role
            set display_name = 'Purchasing'
            where name = 'ROLE_purchasing';
        end if;
    END ;
$$;
