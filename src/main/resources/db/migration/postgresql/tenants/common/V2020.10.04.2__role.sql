create table if not exists auth.role
(
    id            bigserial primary key,
    name          varchar unique not null,
    description   varchar,
    display_order integer        not null,
    created_at    timestamp with time zone default now()
);
