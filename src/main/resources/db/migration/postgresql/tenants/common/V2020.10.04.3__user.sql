create table if not exists auth.user
(
    id           bigserial primary key,
    username     varchar unique not null,
    display_name varchar        not null,
    password     varchar        not null,
    ip_address   varchar,
    logged_at    timestamp with time zone,
    is_active    boolean        not null  default true,
    created_at   timestamp with time zone default now()
);
