update auth.role
set description = '1. Create project. 2. Create project manager'
where name = 'ROLE_admin';

update auth.role
set description   = '1. Manage project level configuration. 2. Manage contract terms. 3. Manage project level users'
  , display_order = 10
where name = 'ROLE_project_manager';

update auth.role
set description   = '1. Manage corporate level reconcile. 2. View project level accountants'' pages',
    display_order = 30
where name = 'ROLE_corporate_accountant';

update auth.role
set description   = '1. Combination of Accountant function and Foreman function'
  , display_order = 50
where name = 'ROLE_accountant_foreman';

update auth.role
set description   = '1. Daily time sheet operation including allocate cost code, report rain out, manage exception and manage per diem if manual. 2. Punch record upload. 3. Project roster management. Daily timesheet approval. 4. Weekly timesheet processing including reconciling OT and per diem'
  , display_order = 70
where name = 'ROLE_accountant';

update auth.role
set description   = '1. Daily timesheet operation including allocate cost code, manage exception and manage per diem. 2. Daily time reporting'
  , display_order = 90
where name = 'ROLE_foreman';
