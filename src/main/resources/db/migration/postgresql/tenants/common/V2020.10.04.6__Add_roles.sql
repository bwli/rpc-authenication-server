INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_admin', 0)
ON CONFLICT DO NOTHING;

INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_project_manager', 1)
ON CONFLICT DO NOTHING;

INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_corporate_accountant', 2)
ON CONFLICT DO NOTHING;

INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_accountant_foreman', 3)
ON CONFLICT DO NOTHING;

INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_accountant', 4)
ON CONFLICT DO NOTHING;

INSERT INTO auth.role (name, display_order)
VALUES ('ROLE_foreman', 5)
ON CONFLICT DO NOTHING;
