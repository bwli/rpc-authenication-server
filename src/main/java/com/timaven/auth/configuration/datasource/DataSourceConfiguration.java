package com.timaven.auth.configuration.datasource;

import com.timaven.auth.configuration.tenant.TenantProperties;
import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.stream.Stream;


@Configuration
public class DataSourceConfiguration {

    private final TenantProperties tenantProperties;
    private final Environment env;
    private final MasterDataSourceProperties masterDataSourceProperties;

    public DataSourceConfiguration(TenantProperties tenantProperties, Environment env,
                                   MasterDataSourceProperties masterDataSourceProperties) {
        this.tenantProperties = tenantProperties;
        this.env = env;
        this.masterDataSourceProperties = masterDataSourceProperties;
    }

    @Bean
    @Primary // If there are multiple DataSources, @Primary is required
    public DataSource dataSource() {
        TenantRoutingDataSource customDataSource = new TenantRoutingDataSource();
        customDataSource.setTargetDataSources(tenantProperties.getDatasources());
        customDataSource.setDefaultTargetDataSource(tenantProperties.getDatasources().get("tenant1"));
        return customDataSource;
    }

    @Bean("clientDetailsDataSource")
    public DataSource clientDetailsDataSource() {
        return this.masterDataSourceProperties.getMasterDataSource();
    }

    @PostConstruct
    public void migrate() {
        if (Arrays.asList(env.getActiveProfiles()).contains("rpc") || Arrays.asList(env.getActiveProfiles()).contains("test")) return;
        tenantProperties
                .getDatasources()
                .values()
                .stream()
                .map(dataSource -> (DataSource) dataSource)
                .forEach(d -> migrate(d, "classpath:/db/migration/postgresql/tenants"));
        migrate(masterDataSourceProperties.getMasterDataSource(),
                "classpath:/db/migration/postgresql/master");
    }

    private void migrate(DataSource dataSource, String locationPrefix) {
        String[] activatedProfiles = env.getActiveProfiles();
        String[] locations = Stream.concat(Arrays.stream(activatedProfiles)
                .map(s -> String.format("%s/%s", locationPrefix, s)),
                Stream.of(String.format("%s/common", locationPrefix))).toArray(String[]::new);

        Flyway flyway = Flyway.configure()
                .dataSource(dataSource)
                .locations(locations)
                .baselineOnMigrate(true)
                .schemas("auth")
                .load();
        flyway.migrate();
    }
}
