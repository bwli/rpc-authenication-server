package com.timaven.auth.configuration.datasource;

import com.timaven.auth.configuration.web.ThreadTenantStorage;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;


public class TenantRoutingDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        String tenantId = ThreadTenantStorage.getTenantId();
//        if (StringUtils.hasText(key)) return key;
//        String tenantId = null;
//        if (RequestContextHolder.getRequestAttributes() != null) {
//            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//            tenantId = (String) attr.getAttribute(TenantRoutingDataSource.SAVED_TENANT_ID, RequestAttributes.SCOPE_REQUEST);
//            // When getSession() is called the first time. System will use this primary datasource to create EntityManager
//            // At this moment, we do not have the tenantId yet (TenantId always after session creation)
////            if (!StringUtils.hasText(tenantId)) {
////                throw new IllegalTenantException("Couldn't decide datasource base on current session");
////            }
//        }
        return tenantId;
    }
}
