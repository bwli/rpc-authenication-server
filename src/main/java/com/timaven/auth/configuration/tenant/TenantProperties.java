package com.timaven.auth.configuration.tenant;

import com.timaven.auth.model.dto.TenantDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@ConfigurationProperties(prefix = "multi-tenants")
public class TenantProperties {

    private Map<Object, Object> datasources = new LinkedHashMap<>();

    private Map<String, TenantDto> tenants = new LinkedHashMap<>();

    private Map<String, String> hosts = new HashMap<>();

    public Map<String, TenantDto> getTenants() {
        return tenants;
    }

    public void setTenants(Map<String, Map<String, String>> tenants) {
        tenants.forEach((key, value) -> this.tenants.put(key, parseTenant(key, value)));
        hosts = this.tenants.values().stream()
                .collect(Collectors.toMap(TenantDto::getHost, TenantDto::getName));
    }

    private TenantDto parseTenant(String key, Map<String, String> source) {
        TenantDto tenantDto = new TenantDto();
        tenantDto.setName(key);
        tenantDto.setHost(source.get("host"));
        tenantDto.setTitle(source.get("title"));
        tenantDto.setDescription(source.get("description"));
        tenantDto.setUrl(source.get("url"));
        tenantDto.setIconSrc(source.get("icon-src"));
        return tenantDto;
    }

    public Map<String, String> getHosts() {
        return hosts;
    }

    public Map<Object, Object> getDatasources() {
        return datasources;
    }

    // Map application.yml tenants -> datasources
    public void setDatasources(Map<String, Map<String, String>> datasources) {
        datasources
                .forEach((key, value) -> this.datasources.put(key, convert(value)));
    }

    public DataSource convert(Map<String, String> source) {
        return DataSourceBuilder.create()
                .url(source.get("url"))
                .driverClassName(source.get("driver-class-name"))
                .username(source.get("username"))
                .password(source.get("password"))
                .build();
    }
}
