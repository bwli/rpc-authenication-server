package com.timaven.auth.service.impl;

import com.timaven.auth.configuration.datasource.TenantRoutingDataSource;
import com.timaven.auth.configuration.web.ThreadTenantStorage;
import com.timaven.auth.dao.repositories.UserRepository;
import com.timaven.auth.lang.IllegalTenantException;
import com.timaven.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service("customUserDetailsService")
@Transactional
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository mUserRepository;

    @Autowired
    public CustomUserDetailsServiceImpl(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws
            UsernameNotFoundException {
        User user = mUserRepository.findValidUserByName(username);
        if (null == user) {
            throw new UsernameNotFoundException("User " + username + " is invalid in database");
        } else {
            if (RequestContextHolder.getRequestAttributes() != null) {
                ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
                // /oauth/authorize
//                String tenantId = (String) attr.getAttribute(TenantRoutingDataSource.SAVED_TENANT_ID, RequestAttributes.SCOPE_REQUEST);
//                if (null == tenantId) {
                    // /auth/me  tenantId was set in TenantAwareJwtAccessTokenConverter
                String tenantId = ThreadTenantStorage.getTenantId();
//                }
                if (!StringUtils.hasText(tenantId)) {
                    throw new IllegalTenantException("Missing tenantId exception");
                }
                user.setTenantId(tenantId);
            }
        }
        return user;
    }
}
