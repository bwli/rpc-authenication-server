package com.timaven.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

@Controller
@PreAuthorize("isAuthenticated()")
public class AnyAuthenticatedController extends BaseController {

    @Autowired
    public AnyAuthenticatedController() {
    }
}
