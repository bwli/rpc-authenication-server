package com.timaven.auth.controller;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Controller
public class GlobalController {

    @ExceptionHandler(value = Exception.class)
    public String handleAllExceptions(Exception ex, WebRequest request) throws Exception {
        ex.printStackTrace();
        if (ex instanceof AccessDeniedException) {
            throw ex;
        } else {
            return "redirect:error?message=" + ex.getClass().getSimpleName();
        }
    }
}
