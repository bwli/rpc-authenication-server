package com.timaven.auth.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    @RequestMapping(value = "/error")
    public ModelAndView renderErrorPage(HttpServletRequest request, @RequestParam(required = false) String message) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        ModelAndView errorPage = new ModelAndView("error");
        String errorMsg = "Unknown error code";

        if (null != status) {
            int httpErrorCode = Integer.valueOf(status.toString());
            switch (httpErrorCode) {
                case 400: {
                    errorMsg = "Http Error Code: 400. Bad Request";
                    break;
                }
                case 401: {
                    errorMsg = "Http Error Code: 401. Unauthorized";
                    break;
                }
                case 404: {
                    errorMsg = "Http Error Code: 404. Resource not found";
                    break;
                }
                case 500: {
                    errorMsg = "Http Error Code: 500. Internal Server Error";
                    break;
                }
            }
        }
        errorPage.addObject("errorMsg", errorMsg);
        String cause = "Cause: " + (StringUtils.isEmpty(message) ? "unknown" : message);
        errorPage.addObject("cause", cause);
        return errorPage;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
