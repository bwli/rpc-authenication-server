package com.timaven.auth.security.sso.config;

import com.timaven.auth.configuration.tenant.TenantProperties;
import com.timaven.auth.configuration.web.ThreadTenantStorage;
import com.timaven.auth.lang.IllegalTenantException;
import com.timaven.auth.model.User;
import com.timaven.auth.model.dto.TenantDto;
import com.timaven.auth.util.Aes;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

@Component
public class TenantAwareFilter implements Filter {

    private final TenantProperties tenantProperties;

    private static final String SPRING_SECURITY_FORM_SERVICE_KEY = "service";
    private static final String SAVED_REQUEST = "SPRING_SECURITY_SAVED_REQUEST";

    public TenantAwareFilter(TenantProperties tenantProperties) {
        this.tenantProperties = tenantProperties;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        String redirectUrl;
        String tenantId = null;
        if (RequestContextHolder.getRequestAttributes() != null) {
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(false);
            // Using jdbc session will change the session attribute storage location
//            HttpSession session = null;
//            if (request instanceof HttpServletRequest) {
//                session = ((HttpServletRequest) request).getSession(false);
//            }
            if (null != session && null != session.getAttribute(SAVED_REQUEST)) {
                // /auth/login
                redirectUrl = ((DefaultSavedRequest) session.getAttribute(SAVED_REQUEST)).getRedirectUrl();
                MultiValueMap<String, String> parameters = UriComponentsBuilder.fromUriString(redirectUrl)
                        .build().getQueryParams();
                String redirectUri = parameters.getFirst("redirect_uri");
                tenantId = getTenantIdFromUri(redirectUri);
            } else if (StringUtils.hasText(attr.getRequest().getParameter("redirect_uri"))) {
                // /auth/authorize /auth/token
                String redirectUri = attr.getRequest().getParameter("redirect_uri");
                tenantId = getTenantIdFromUri(redirectUri);
            }
        }

        if (tenantId == null) {
            // /login after session timeout
            String service = request.getParameter(SPRING_SECURITY_FORM_SERVICE_KEY);
            if (StringUtils.hasText(service)) {
                Key aesKey = new SecretKeySpec(Aes.key.getBytes(), "AES");
                Cipher cipher;
                try {
                    cipher = Cipher.getInstance("AES");
                    cipher.init(Cipher.DECRYPT_MODE, aesKey);
                    tenantId = new String(cipher.doFinal(Hex.decodeHex(service.toCharArray())));
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | DecoderException e) {
                    e.printStackTrace();
                }
                if (tenantId == null) {
                    throw new IllegalTenantException("Couldn't decide datasource base on current session");
                } else {
                    TenantDto dto = tenantProperties.getTenants().get(tenantId);
                    if (dto != null) {
                        HttpServletResponse httpResponse = (HttpServletResponse) response;
                        httpResponse.sendRedirect(dto.getUrl());
                        return;
                    }
                }
            }
        }

        if (tenantId != null) {
            // Prevent Cross Tenant with same session
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (null != authentication && authentication.getPrincipal() != null
                    && authentication.getPrincipal() instanceof User) {
                User principal = (User) authentication.getPrincipal();
                String authenticatedTenantId = principal.getTenantId();
                if (authenticatedTenantId != null && !authenticatedTenantId.equalsIgnoreCase(tenantId)) {
                    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
                    String absoluteContextRootURL = httpServletRequest.getRequestURL().toString().replace(httpServletRequest.getRequestURI().substring(1), httpServletRequest.getContextPath());
                    UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(String.format("%s/login?service=%s", absoluteContextRootURL, Aes.encryptService(tenantId))).build();
                    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                    HttpSession session = httpServletRequest.getSession();
                    session.invalidate();
                    httpServletResponse.sendRedirect(uriComponents.toUriString());
                    return;
                }
            }

//            HttpSession session;
//            if (request instanceof HttpServletRequest) {
//                session = ((HttpServletRequest) request).getSession(false);
//                if (session != null) {
//                    session.setAttribute(SAVED_TENANT_ID, tenantId);
//                }
//            }

            ThreadTenantStorage.setTenantId(tenantId);
        }

        chain.doFilter(request, response);
    }

    private String getTenantIdFromUri(String redirectUri) {
        try {
            URI uri;
            if (redirectUri != null) {
                uri = new URI(redirectUri);
                String host = uri.getHost();
                int port = uri.getPort();
                if (port != -1) {
                    host = host + ":" + port;
                }
                return tenantProperties.getHosts().get(host);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return "";
    }
}
