package com.timaven.auth.security.sso.config;

import com.timaven.auth.security.sso.oauth2.CustomAuthenticationFailureHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder mPasswordEncoder;
    private final UserDetailsService mUserDetailsService;
    private final Environment env;
    private final TenantAwareFilter tenantAwareFilter;

//    private final SecurityAuthSuccessHandler mAuthSuccessHandler;

    //    @Bean
//    public AjaxAwareAuthenticationEntryPoint unauthorizedHandler() {
//        return new AjaxAwareAuthenticationEntryPoint("/login");
//    }
    @Autowired
    public WebSecurityConfig(@Qualifier("customUserDetailsService") UserDetailsService userDetailsService,
                             PasswordEncoder passwordEncoder, Environment env, TenantAwareFilter tenantAwareFilter) {
        mUserDetailsService = userDetailsService;
        mPasswordEncoder = passwordEncoder;
//        mAuthSuccessHandler = authSuccessHandler;
        this.env = env;
        this.tenantAwareFilter = tenantAwareFilter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // The Redirection Endpoint is used by the Authorization Server for returning the Authorization Response
        // (which contains the authorization credentials) to the client via the Resource Owner user-agent.
        http
                .addFilterBefore(tenantAwareFilter, UsernamePasswordAuthenticationFilter.class)
                .csrf().ignoringAntMatchers("/logout**")
                .and()
                .requestMatchers()
                .antMatchers("/", "/index", "/error", "/login**", "/logout", "/oauth/authorize", "/oauth/confirm_access")
                .and()
                .authorizeRequests()
                .antMatchers("/login**", "/logout**", "/index", "/error").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .failureHandler(authenticationFailureHandler())
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .logoutSuccessHandler(((new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))));

//        String[] activatedProfiles = env.getActiveProfiles();
//        if (Arrays.asList(activatedProfiles).contains("rpc")) {
        http.cors().configurationSource(configurationSource());
//        }
    }

    private CorsConfigurationSource configurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
//        config.addAllowedOrigin("*");
        config.addAllowedOriginPattern("*");
        config.setAllowCredentials(true);
        config.addAllowedHeader("X-Requested-With");
        config.addAllowedHeader("Content-Type");
        config.addAllowedMethod(HttpMethod.POST);
        source.registerCorsConfiguration("/logout", config);
        return source;
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/**", "/images/**", "/js/**", "/actuator/**");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(mUserDetailsService);
        authenticationProvider.setPasswordEncoder(mPasswordEncoder);
        return authenticationProvider;
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
}
