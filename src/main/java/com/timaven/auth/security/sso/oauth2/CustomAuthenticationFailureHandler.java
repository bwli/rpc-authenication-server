package com.timaven.auth.security.sso.oauth2;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private static final String SPRING_SECURITY_FORM_SERVICE_KEY = "service";

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        if (exception.getMessage().contains("IllegalTenantException")) {
            setDefaultFailureUrl("/login?error=IllegalTenantException");
        } else {
            String service = request.getParameter(SPRING_SECURITY_FORM_SERVICE_KEY);
            setDefaultFailureUrl(String.format("/login?service=%s&error=", service));
        }
        super.onAuthenticationFailure(request, response, exception);
    }
}
